import { PocAngularCliPage } from './app.po';

describe('poc-angular-cli App', function() {
  let page: PocAngularCliPage;

  beforeEach(() => {
    page = new PocAngularCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
